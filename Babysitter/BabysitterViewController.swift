//
//  BabysitterViewController.swift
//  Babysitter
//
//  Created by Henry Shilling on 9/16/18.
//  Copyright © 2018 Henry Shilling. All rights reserved.
//

import UIKit

class BabysitterViewController: UIViewController {
    @IBOutlet weak var PaymentTotalField: UITextField!
    @IBOutlet weak var StartField: UITextField!
    @IBOutlet weak var BedTimeField: UITextField!
    @IBOutlet weak var EndTimeField: UITextField!
    
    var startTime: Date?
    var bedTime: Date?
    var endTime: Date?
    var selectedTag = 0
    var minimumDate: Date?
    let timePayment = TimeAndPayment()
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        // Only show the total if all fields are filled in
        if startTime != nil && bedTime != nil && endTime != nil {
            let cost: Int = timePayment.CalculateCost(startTime: startTime!, bedTime: bedTime!, endTime: endTime!)
            PaymentTotalField.text = "$" + String(cost) + ".00"
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?)  {
        let vc = segue.destination as? DatePickerViewController
        vc?.selectedTag = selectedTag
        vc?.minimumDate = minimumDate
    }
    
    @IBAction func unwindToTop(segue: UIStoryboardSegue) {
    }
}

extension BabysitterViewController: UITextFieldDelegate
{
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        selectedTag = textField.tag
        
        switch selectedTag {
        case 1:
            minimumDate = startTime
        case 2:
            minimumDate = bedTime
        default:
            minimumDate = nil
        }
        
        performSegue(withIdentifier: "DatePickerSegue", sender: self)
        return false
    }
}

extension BabysitterViewController: DateSelected
{
    func setDate(date: Date) {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd-MM hh:mm"
        
        switch selectedTag {
        case 0:
            startTime = date
            StartField.text = dateFormatter.string(from: date)
        case 1:
            bedTime = date
            BedTimeField.text = dateFormatter.string(from: date)
        case 2:
            endTime = date
            EndTimeField.text = dateFormatter.string(from: date)
        default:
            PaymentTotalField.text = "Input Error"
            return
        }
    }
}


