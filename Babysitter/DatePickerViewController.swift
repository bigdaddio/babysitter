//
//  DatePickerViewController.swift
//  Babysitter
//
//  Created by Henry Shilling on 9/16/18.
//  Copyright © 2018 Henry Shilling. All rights reserved.
//

import UIKit

class DatePickerViewController: UIViewController {

    @IBOutlet var titleLabel: UILabel!
    @IBOutlet weak var datePicker: UIDatePicker!
    
    var selectedTag: Int = 0
    var minimumDate: Date!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        switch selectedTag {
        case 0:
            self.titleLabel.text = "Set start Time"
        case 1:
            self.titleLabel.text = "Set bed Time"
        case 2:
            self.titleLabel.text = "Set end Time"
        default:
            self.titleLabel.text = "Shouldn't happen"
        }
        
        if minimumDate != nil {
            datePicker.minimumDate = minimumDate.addingTimeInterval(TimeInterval(3600))
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func DoneSelected(_ sender: Any) {
        performSegue(withIdentifier: "UnwindToMain", sender: self)
    }
    
    // MARK: - Navigation
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let vc = segue.destination as? BabysitterViewController
        vc?.setDate(date: RoundOffSeconds(date: datePicker.date))
    }
    
    // MARK: Helper function
    
    func RoundOffSeconds(date: Date) -> Date {
        let timeInterval = floor(date .timeIntervalSinceReferenceDate / 60.0) * 60.0
        return Date(timeIntervalSinceReferenceDate: timeInterval)
    }

}
