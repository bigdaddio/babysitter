//
//  DateSelectedPProtocol.swift
//  Babysitter
//
//  Created by Henry Shilling on 9/16/18.
//  Copyright © 2018 Henry Shilling. All rights reserved.
//

import Foundation

protocol DateSelected
{
    func setDate(date: Date)
}

