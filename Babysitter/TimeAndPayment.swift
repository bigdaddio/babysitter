//
//  TimeAndPayment.swift
//  Babysitter
//
//  Created by Henry Shilling on 9/16/18.
//  Copyright © 2018 Henry Shilling. All rights reserved.
//

import Foundation

class TimeAndPayment  {
    let normalPrice: Int = 12
    let bedPrice: Int = 8
    let latePrice: Int = 16
    
    func NumberOfHours(startTime: Date, endTime: Date) -> Int {
        let timeElapsed = endTime.timeIntervalSince(startTime)
        return Int(ceil(timeElapsed / 3600))
    }
    
    func CalculateCost(startTime: Date, bedTime: Date, endTime: Date) -> Int
    {
        let cal = Calendar(identifier: .gregorian)
        var midnight = cal.startOfDay(for: startTime)
        midnight = midnight.addingTimeInterval(24 * 3600) // add 24 hours.
        
        var totalCost: Int = NumberOfHours(startTime: startTime, endTime: bedTime) * normalPrice
        totalCost += NumberOfHours(startTime: bedTime, endTime: midnight) * bedPrice
        if endTime > midnight {
            totalCost += NumberOfHours(startTime: midnight, endTime: endTime) * latePrice
        }
        
        return totalCost
    }
}
