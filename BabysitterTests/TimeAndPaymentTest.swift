//
//  TimeAndPaymentTest.swift
//  BabysitterTests
//
//  Created by Henry Shilling on 9/16/18.
//  Copyright © 2018 Henry Shilling. All rights reserved.
//

import XCTest
@testable import Babysitter

class TimeAndPaymentTest: XCTestCase {
    
    var startTime: Date!
    var bedTime: Date!
    var endTime: Date!
    var midnight: Date!
    let timePayment = TimeAndPayment()
    
    override func setUp() {
        super.setUp()
        
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy/MM/dd HH:mm"
        
        startTime = formatter.date(from: "2018/01/01 17:00")
        bedTime = formatter.date(from: "2018/01/01 18:00")
        endTime = formatter.date(from: "2018/01/02 04:00")
        midnight = formatter.date(from: "2018/01/02 00:00")
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
        
        startTime = nil
        bedTime = nil
        endTime = nil
        midnight = nil
    }
    
    func testHoursCalculator() {
        var hours = timePayment.NumberOfHours(startTime: startTime, endTime: bedTime)
        XCTAssertTrue((hours > 0) && (hours < 7))
        hours = timePayment.NumberOfHours(startTime: bedTime, endTime: midnight)
        XCTAssertTrue((hours >= 0) && (hours < 7))
        hours = timePayment.NumberOfHours(startTime: midnight, endTime: endTime)
        XCTAssertTrue((hours >= 0) && (hours <= 4))
    }
    
    func testTotalCost() {
        let cost: Int = timePayment.CalculateCost(startTime: startTime, bedTime: bedTime, endTime: endTime)
        XCTAssertTrue(cost > 0)
    }
}
