# Babysitter kata

#background
Who even knows what a kata is, this is the first I have ever heard of one in programming or any sense.

Built following the rules set forth in https://github.com/PillarTechnology/kata-babysitter/blob/master/README.md

Issues: I may have made it too fancy, setting the minimum time of the start would take a bit more work. I didnt want to assume the current day is the day you want to calculate for. So in order to create a minimum for the date picker I would need at least a date. I guess I could make it so the minimum is 5:00 pm for the date selected after a person selects a date. This would not allow the user to set the time and then the date.

I didnt really spend all that long building this. It was a thing I worked on between other stuff I had to do. Like Make a cake.